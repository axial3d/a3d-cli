#!/bin/bash
set -ex

black a3d_cli/ tests/
isort a3d_cli/ tests/
unify --in-place --recursive --quote \" a3d_cli/ tests/
