#!/bin/bash
set -ex

poetry run black --check --diff a3d_cli/ tests/
poetry run pylint a3d_cli/  tests/
poetry run isort --check-only --diff a3d_cli/  tests/
poetry run unify --check-only --recursive --quote \" a3d_cli/  tests/
poetry check
poetry show --outdated
