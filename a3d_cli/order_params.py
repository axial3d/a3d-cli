scan_type = {
    "ct": {"scanType": {"id": 1, "name": "CT"}},
    "mri": {"scanType": {"id": 2, "name": "MRI"}},
    "pet": {"scanType": {"id": 3, "name": "PET"}},
    "spect": {"scanType": {"id": 4, "name": "SPECT"}},
}

tissue_type = {
    "hard": {"modelBoneType": {"id": 1, "name": "Hard"}},
    "soft": {"modelBoneType": {"id": 2, "name": "Soft"}},
}
