import pytest


def pytest_addoption(parser):
    parser.addoption("--integration", action="store_true", default=False, help="run integration tests")
    parser.addoption("--integration-user", default=None, help="Integration tests user")
    parser.addoption("--integration-password", default=None, help="Integration tests password")
    parser.addoption("--integration-url", default=None, help="Integration tests URL")


def pytest_configure(config):
    config.addinivalue_line("markers", "integration: mark test as integration")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--integration"):
        url = config.option.integration_url
        username = config.option.integration_user
        password = config.option.integration_password

        if url is None:
            pytest.fail("Must set --integration-url for integration tests")

        if username is None:
            pytest.fail("Must set --integration-user for integration tests")

        if password is None:
            pytest.fail("Must set --integration-password for integration tests")

        return

    skip_integration = pytest.mark.skip(reason="need --integration option to run")
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)
