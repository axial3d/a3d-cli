import json
import tempfile
from datetime import datetime, timedelta
from os.path import dirname, join

import pytest

from a3d_cli.cli import cli
from a3d_cli.interface import A3dCliInterface

pytestmark = pytest.mark.integration


TEST_DIRECTORY = join(dirname(__file__), "testdata")


@pytest.mark.integration
def test_cli(request):

    url = request.config.option.integration_url
    username = request.config.option.integration_user
    password = request.config.option.integration_password

    with tempfile.NamedTemporaryFile() as temp_config_file:

        temp_config_file.write(json.dumps({"orders": {}}).encode("utf-8"))

        temp_config_file.flush()

        required_date = datetime.utcnow() + timedelta(days=10)

        order_id = cli(
            "create",
            username,
            password,
            order_types=["3DMesh"],
            catalogue_item_id=101,
            required_date=required_date.strftime("%d-%m-%y"),
            gender="U",
            birth_year=2021,
            surgery="surgery",
            notes="notes",
            base_url=url,
            dicom_location=TEST_DIRECTORY,
            verify=False,
        )

        cli(
            "upload",
            username,
            password,
            dicom_location=TEST_DIRECTORY,
            base_url=url,
            order_id=str(order_id),
            verify=False,
        )


@pytest.mark.integration
def test_interface(request):  # pylint: disable=too-many-statements

    url = request.config.option.integration_url
    username = request.config.option.integration_user
    password = request.config.option.integration_password

    a3d = A3dCliInterface(base_url=url, verify=False)

    a3d.login(username, password)

    assert a3d.jwt

    required_date = datetime.utcnow() + timedelta(days=10)

    order = a3d.create_order(
        order_type_ids=[1],
        catalogue_item_id=100,
        required_date=required_date,
        gender="U",
        birth_year=2021,
        surgery="Test",
        notes="Test",
    )
    assert int(order["id"])
    assert int(order["userId"])
    assert order["notes"] == "Test"
    assert order["surgery"] == "Test"
    assert order["patientBirthYear"] == 2021
    assert order["patientGender"] == "U"
    assert datetime.fromisoformat(order["requiredDate"])
    assert order["statusId"] == 0
    assert order["catalogueItemId"] == 100
    assert order["deliveryDetailsId"] == 0
    assert order["invoiceDetailsId"] == 0
    assert order["orderTypeIds"] == [1]

    a3d.upload_files(TEST_DIRECTORY)
    a3d.complete_upload()

    fetched_order = a3d.get_order(order["id"])

    assert int(fetched_order["id"])
    assert int(fetched_order["userId"])
    assert int(fetched_order["statusId"])
    assert fetched_order["notes"] == "Test"
    assert fetched_order["surgery"] == "Test"
    assert fetched_order["patientBirthYear"] == 2021
    assert fetched_order["patientGender"] == "Undisclosed"
    assert datetime.fromisoformat(fetched_order["requiredDate"])
    assert fetched_order["deliveryDetailsId"] == 0
    assert fetched_order["invoiceDetailsId"] == 0

    catalogue_item = fetched_order["catalogueItem"]
    assert catalogue_item["label"] is None
    assert catalogue_item["name"] is None
    assert catalogue_item["description"] is None
    assert catalogue_item["categoryId"] == 0
    assert catalogue_item["imageUrls"] == []
    assert fetched_order["orderTypes"] == [
        {"description": None, "id": 1, "label": "3D Visual", "name": "3DVisual", "notes": None}
    ]

    a3d.update_order(order=order)
    updated_order = a3d.get_order(order["id"])

    assert int(updated_order["id"])
    assert int(updated_order["userId"])
    assert int(updated_order["statusId"])
    assert updated_order["notes"] == "Test"
    assert updated_order["surgery"] == "Test"
    assert updated_order["patientBirthYear"] == 2021
    assert updated_order["patientGender"] == "Undisclosed"
    assert datetime.fromisoformat(updated_order["requiredDate"])
    catalogue_item = updated_order["catalogueItem"]
    assert catalogue_item["label"] is None
    assert catalogue_item["name"] is None
    assert catalogue_item["description"] is None
    assert catalogue_item["categoryId"] == 0
    assert catalogue_item["imageUrls"] == []
    assert updated_order["deliveryDetailsId"] == 0
    assert updated_order["invoiceDetailsId"] == 0
    assert updated_order["orderTypes"] == [
        {"description": None, "id": 1, "label": "3D Visual", "name": "3DVisual", "notes": None}
    ]
