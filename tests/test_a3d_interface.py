from a3d_cli import __version__
from a3d_cli.cli import cli


def test_version():
    assert __version__


def test_cli(mocker):

    mock_json = mocker.patch("a3d_cli.interface.json")
    mocker.patch("a3d_cli.cli.check_error_response", lambda r: r)

    check_dicom_location = mocker.patch("a3d_cli.interface.A3dCliInterface.check_dicom_location")
    upload_files = mocker.patch("a3d_cli.interface.A3dCliInterface.upload_files")
    fetch_order_types = mocker.patch(
        "a3d_cli.interface.A3dCliInterface.fetch_order_types", return_value={"3DMesh": {"id": 1}}
    )
    fetch_catalogue = mocker.patch(
        "a3d_cli.interface.A3dCliInterface.fetch_catalogue", return_value={101: {"id": 101, "name": "heart"}}
    )
    requests = mocker.patch(
        "a3d_cli.interface.requests",
        mocker.Mock(
            post=mocker.Mock(
                name="requests_post",
                return_value=mocker.Mock(
                    name="requests_post_return", json=mocker.Mock(side_effect=[{"jwt": "jwt"}, {"id": 999}, {"y", "y"}])
                ),
            ),
            get=mocker.Mock(
                name="requests_get",
                return_value=mocker.Mock(
                    name="requests_get_return",
                    json=mocker.Mock(
                        side_effect=[
                            {"id": 999, "displayOrderNo": "displayOrderNo999"},
                        ]
                    ),
                ),
            ),
            name="requests",
        ),
    )

    order_id = cli(
        "create",
        "username",
        "password",
        order_types=["3DMesh"],
        catalogue_item_id=101,
        required_date="2021-01-01",
        gender="U",
        birth_year=2021,
        surgery="surgery",
        notes="notes",
        base_url="http://127.0.0.1/base_url",
        dicom_location="test_directory",
        verify=False,
    )

    assert order_id

    assert check_dicom_location.mock_calls == [mocker.call("test_directory")]

    assert upload_files.mock_calls == [mocker.call("test_directory")]
    assert fetch_order_types.mock_calls == [mocker.call()]
    assert fetch_catalogue.mock_calls == [mocker.call()]

    assert requests.get.mock_calls == [
        mocker.call(
            "http://127.0.0.1/base_url/api/v3/orders/999",
            mock_json.dumps.return_value,
            files=None,
            verify=False,
            headers={
                "User-Agent": "a3dcli %s" % __version__,
                "Accept": "*/*",
                "Accept-Encoding": "gzip, deflate, br",
                "Authorization": "Bearer jwt",
                "Content-Type": "application/json;",
            },
        ),
    ]

    assert requests.post.mock_calls == [
        mocker.call(
            "http://127.0.0.1/base_url/api/v2/login",
            json={"username": "username", "password": "password"},
            verify=False,
        ),
        mocker.call(
            "http://127.0.0.1/base_url/api/v3/orders",
            mock_json.dumps.return_value,
            files=None,
            verify=False,
            headers={
                "User-Agent": "a3dcli %s" % __version__,
                "Accept": "*/*",
                "Accept-Encoding": "gzip, deflate, br",
                "Authorization": "Bearer jwt",
                "Content-Type": "application/json;",
            },
        ),
        mocker.call(
            "http://127.0.0.1/base_url/api/v3/orders/999/uploadComplete",
            mock_json.dumps.return_value,
            files=None,
            verify=False,
            headers={
                "User-Agent": "a3dcli %s" % __version__,
                "Accept": "*/*",
                "Accept-Encoding": "gzip, deflate, br",
                "Authorization": "Bearer jwt",
                "Content-Type": "application/json;",
            },
        ),
    ]
